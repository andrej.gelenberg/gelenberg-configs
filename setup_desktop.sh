#!/bin/sh

. ./setup.sh

set +x

deploy_systemd_unit() {
  clns "$1" ~/".config/systemd/user/$1"
}

set -xe

clns i3_config     ~/.config/i3/config
clns sway_config   ~/.config/sway/config
clns i3status.conf ~/.config/i3status/config
clns i3status.toml ~/.config/i3status/config.toml
clns Xresources    ~/.Xresources
clns kitty.conf    ~/.config/kitty/kitty.conf
clns gitconfig     ~/.gitconfig
clns yt-dlp.conf   ~/.yt-dlp/config

for i in *.service *.target *.service.d
do
  deploy_systemd_unit "$i"
done

systemctl --user daemon-reload

for i in ssh-agent.service xiccd.service
do
  systemctl --user enable $i
done

systemctl --user add-wants wm.target blueman-applet.service xiccd.service
