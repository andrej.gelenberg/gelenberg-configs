#!/bin/sh

echo "setup usage of of the configs from this repo"

basedir="$(realpath "$(dirname "$4")")"

echo "basedir: $basedir"

echo "set home dir links"

bashrc_inc=". \"$basedir/bashrc\""
bash_profile_inc=". \"$basedir/bash_profile\""

clns() {
  from="$1"
  to="$2"

  mkdir -p "$(dirname "$to")"
  ln -sfn "$basedir/$from" "$to"
}

set -xe

make

grep -qF "$bashrc_inc"       ~/.bashrc || echo "$bashrc_inc"       >> ~/.bashrc
grep -qF "$bash_profile_inc" ~/.bashrc || echo "$bash_profile_inc" >> ~/.bash_profile

clns tmux.conf   ~/.tmux.conf
clns nvim        ~/.config/nvim
clns vimrc       ~/.vimrc
clns inputrc     ~/.inputrc
clns config.fish ~/.config/fish/conf.d/gelenberg-config.fish

if [ ! -e ~/.local/tmp ]
then
  mkdir ~/.local
  mkdir -m 0700 ~/.local/tmp
fi
