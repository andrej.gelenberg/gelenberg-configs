#!/usr/bin/ruby
# frozen_string_literal: true

require_relative 'i3_common'

input 'type:keyboard' do
  xkb_layout  'us,de,ru'
  xkb_options 'grp:alts_toggle'
end

output '*' do
  bg '~/.bg', :fill
end

output 'Dell Inc. DELL U2412M 0FFXD31M0F5S' do
  pos 0,    0
  res 1920, 1200
end

output 'BNQ BenQ GL2450H N3E00999SL0' do
  pos 1920, 0
  res 1920, 1080
end

output 'eDP1' do
  pos 3840, 0
end

output 'LG Electronics 24EB23 512NTZN28909' do
  pos 0, 0
end

output 'BNQ BenQ SW240 8CK02289SL0' do
  pos 1920, 0
end


exec '~/bin/wlstart'
