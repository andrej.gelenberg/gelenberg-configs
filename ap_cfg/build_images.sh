#!/bin/bash

VERSION=24.10.0
PACKAGES="\
  luci luci-ssl luci-app-opkg luci-mod-admin-full luci-mod-network \
  luci-mod-status luci-mod-system luci-proto-ipv6 \
  luci-app-statistics luci-app-uhttpd  luci-theme-material \
  luci-app-usteer usteer luci-app-vnstat2 vnstat2 \
  screen vim wpad-openssl \
  -wpad-basic-mbedtls -dnsmasq -ppp -ppp-mod-pppoe -luci-proto-ppp"

mkimg() {
  local platform="$1"
  local profile="$2"
  podman run --rm \
    -v "./fw:/builder/bin" \
    "docker.io/openwrt/imagebuilder:${platform}-${VERSION}" \
    /bin/sh -c "make PROFILE=\"$profile\" PACKAGES=\"$PACKAGES\" image"
}

set -xe

[ -e fw ] && sudo rm -rf fw
mkdir fw
chmod a+rwx fw

mkimg ath79-generic tplink_archer-c7-v5
mkimg ipq40xx-chromium google_wifi 
