#!/bin/sh

yay -S --needed \
  i3-wm i3lock i3status-rust xss-lock sway swayidle swaylock swaybg \
  rustup \
  stlink stm32flash openocd urjtag flashrom \
  arm-none-eabi-gcc arm-none-eabi-gdb arm-none-eabi-binutils arm-none-eabi-newlib \
  ccls \
  stress \
  zutty \
  firefox google-chrome \
  sx xorg xorg-apps xf86-input-evdev xf86-input-libinput xf86-video-intel xf86-video-amdgpu nvtop \
  wayland weston neatvnc \
  base-devel \
  zeal \
  ansible ansible-language-server ansible-lint \
  arch-install-scripts \
  argyllcms xiccd  \
  aspell aspell-de aspell-ru aspell-en hunspell hunspell-ru hunspell-de hunspell-en_us \
  blueman bluez bluez-cups bluez-tools bluez-utils \
  booster busybox \
  cdrtools dvd+rw-tools udftools \
  usbutils pciutils \
  bind \
  esniper \
  feh geeqie swayimg \
  bc \
  bcc bpf bcc-tools bpftrace \
  bwm-ng nmon iotop powertop iperf3 \
  sshfs openssh clusterssh \
  make cmake gradle ninja \
  cpio \
  valgrind gdb strace ltrace perf kernelshark pahole \
  podman cni-plugins podman-compose podman-docker dockerfile-language-server dockerfile_lint docker-scan \
  codespell \
  cscope ctags \
  linux-tools \
  cups cups-pdf cups-filters dash sane xsane \
  darktable gimp inkscape hugin \
  debuginfod \
  cpupower \
  dmidecode \
  fd fzf \
  fakeroot \
  flex bison \
  fish shellcheck shellharden  \
  bash-language-server \
  lua-language-server \
  yaml-language-server \
  python-lsp-server python-pydocstyle python-pyflakes python-rope flake8 python-whatthepatch \
  vscode-css-languageserver \
  vscode-html-languageserver \
  marksman \
  ripgrep \
  easyeffects \
  ed \
  efibootmgr grub syslinux \
  evince \
  ethtool \
  eslint js-beautify tslint typescript nodejs npm \
  unarj unrar unace \
  uboot-tools \
  exfatprogs \
  freecad \
  freerdp \
  fuse-overlayfs \
  dosfstools \
  git git-lfs git-delta gitlint languagetool-rust \
  gnuplot \
  hexedit \
  go gopls \
  graphviz \
  gstreamer-vaapi \
  hdparm smartmontools nvme-cli \
  gtk2fontsel \
  i7z intel-gpu-tools intel-media-driver intel-ucode \
  amd-ucode amdvlk amdctl radeontool radeontop \
  intellij-idea-ultimate-edition android-studio \
  android-tools \
  blender kdenlive \
  irqbalance \
  sox \
  iwd wireless-regdb \
  jdk-openjdk jdk17-temurin jdtls \
  keepassxc \
  kotlin kotlin-language-server ktlint \
  lshw \
  languagetool \
  libdvdcss \
  libreoffice-fresh \
  linux linux-lts linux-docs linux-firmware \
  kexec-tools \
  man-db man-pages \
  memtest86+-efi \
  mesa mesa-utils mesa-vdpau vdpauinfo \
  vulkan-extra-tools vulkan-intel vulkan-mesa-layers vulkan-swrast vulkan-tools vulkan-radeon \
  mlocate \
  mplayer mpv vlc \
  mtr nmap termshark wireshark-qt wireshark-cli tcpdump traceroute whois socat \
  inotify-tool \
  vi vim neovim ruby-neovim python-pynvim nodejs-neovim global idutils \
  net-tools iproute2 \
  nss-mdns avahi \
  openscad freecad kicad kicad-library prusa-slicer ngspice xyce-serial qucs-s freehdl iverilog gtkwave octave openvaf \
  pipewire pipewire-alsa pipewire-pulse pipewire-v4l2 pipewire-docs pipewire-x11-bell qpwgraph \
  pstoedit \
  psutils \
  pwgen \
  python ruff ruff-lsp \
  qemu-full \
  rclone \
  rsync \
  read-edid \
  reflector \
  ruby rubocop ruby-docs ruby-solargraph ruby-ffi\
  sql-language-server \
  sqlfluff sqls \
  spectre-meltdown-checker \
  stylelint \
  texlive-most texlab texlive-lang \
  thermald \
  thunar \
  thunderbird \
  tmux \
  systemd-resolvconf \
  wol \
  tidy \
  tlp \
  vacuum \
  w3m \
  wine winetricks \
  wireguard-tools \
  xclip \
  yamllint yq \
  yt-dlp \
  ocl-icd pocl intel-graphics-compiler opencl-legacy-amdgpu-pro \
  rdiff-backup \
  acpi \
  xjobs \
  aria2 wget curl \
  radare2 r2ghidra iaito \
  lvm2 \
  llvm \
  p7zip pdftk picocom \
  time \
  waydroid \
  pkgfile \
  diskonaut \
  xmllint lemminx \
  moreutils \
  rasdaemon \
  ddcutils displaycal \
  bridge-utils dmraid \
  libimobiledevice usbmuxd \
  \
  awesome-terminal-fonts \
  gnu-free-fonts \
  ttf-droid \
  ttf-font-awesome \
  ttf-ibm-plex \
  ttf-liberation \
  ttf-linux-libertine \
  ttf-linux-libertine-g \
  ttf-material-design-iconic-font \
  powerline-fonts \
  terminus-font \
  ttf-google-fonts-git \
  ttf-ibmplex-mono-nerd \
  ttf-ionicons \
  ttf-nerd-fonts-symbols-mono \
  ttf-roboto \
  ttf-sourcecodepro-nerd \
  ttf-ubuntu-font-family \
  ttf-bigblueterminal-nerd \
  intel-one-mono-fonts \
  nerd-fonts \

