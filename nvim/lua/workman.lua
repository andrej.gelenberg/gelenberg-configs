local map = {
  { "n", "l", "o" },
  { "n", "o", "l" },
  { "n", "L", "O" },
  { "n", "O", "L" },
  { "n", "j", "n" },
  { "n", "n", "j" },
  { "n", "J", "N" },
  { "n", "N", "J" },
  { "n", "gn", "gj" },
  { "n", "gj", "gn" },
  { "n", "k", "e" },
  { "n", "e", "k" },
  { "n", "K", "E" },
  { "n", "E", "<nop>" },
  { "n", "gk", "ge" },
  { "n", "ge", "gk" },
  { "n", "h", "y" },
  { "o", "h", "y" },
  { "n", "y", "h" },
  { "n", "H", "Y" },
  { "n", "Y", "H" }
}

for i, mapping in ipairs(map) do
  vim.keymap.set(mapping[1], mapping[2], mapping[3], {noremap = true})
end
