local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

return require("lazy").setup({
  'neovim/nvim-lspconfig',
  'j-hui/fidget.nvim',
  'nvim-tree/nvim-tree.lua',
  { 'nvim-treesitter/nvim-treesitter', build = ":TSUpdateSync" },
  'ibhagwan/fzf-lua',
  { 'lukas-reineke/indent-blankline.nvim', main= "ibl", opts = {} },
  'nvim-lua/plenary.nvim',
  'nvim-tree/nvim-web-devicons',
  'nvim-telescope/telescope.nvim',
  { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
  'folke/trouble.nvim',
  'tpope/vim-fugitive',
  'lewis6991/gitsigns.nvim',
  'jose-elias-alvarez/null-ls.nvim',
  {
    'folke/which-key.nvim',
    event = 'VeryLazy',
    init = function()
      vim.o.timeout    = true
      vim.o.timeoutlen = 300
    end,
    opts = {},
  },
  {
    "folke/trouble.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
  },
  'joorem/vim-haproxy',
  'nfnty/vim-nftables'
})
