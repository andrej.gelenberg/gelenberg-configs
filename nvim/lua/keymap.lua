local tsp = require('telescope.builtin')
local Util = require('lazy.util')

local bindings = {
  {'n', {
    {'<leader>', {
      -- telescope
      { 'ff', tsp.find_files,           {}, 'find files'},
      { 'fg', tsp.live_grep,            {}, 'live grep' },
      { 'fb', tsp.buffers,              {}, 'find in buffers' },
      { 'fh', tsp.help_tags,            {}, 'help tags' },
      { 'fr', tsp.resume,               {}, 'resume search' },
      { 'fi', tsp.lsp_implementations,  {}, 'goto implementation'},
      { 'fd', tsp.lsp_type_definitions, {}, 'goto definistion'},
      { 'fw', tsp.lsp_references,       {}, 'goto references'},
      { 'ft', tsp.treesitter,           {}, 'find in treesitter'},

      -- lsp
      { 'cd', vim.diagnostic.open_float, {}, 'Line Diagnostics' },
      { 'cl', ":LspInfo<cr>",            {}, "Lsp Info"},

      -- lazygit
      { 'gg',
        function() Util.float_term({"lazygit"}) end,
        {},
        "Lazygit"
      },
      { 'gl', function() Util.float_term({"lazygit", "log"}) end, {}, "lazygit log"},

      { 'de', vim.diagnostic.enable, {}, 'enable diagnostics' },
      { 'dd', vim.diagnostic.disable, {}, 'enable diagnostics' },

      { 'ip', vim.show_pos, {}, 'inspect position' }
  }}}},
}

for i, v in ipairs(bindings) do
  local mode          = v[1]
  local mode_bindings = v[2]

  for i, v in ipairs(mode_bindings) do
    local leader          = v[1]
    local leader_bindings = v[2]

    for i, v in ipairs(leader_bindings) do
      local key   = v[1]
      local cmd   = v[2]
      local opts   = v[3]
      opts['desc'] = v[4]

      vim.keymap.set(mode, leader .. key, cmd, opts)
    end
  end
end
