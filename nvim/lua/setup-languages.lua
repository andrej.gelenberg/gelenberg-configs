require('nvim-treesitter.configs').setup {
  ensure_installed = {
    "c",
    "vim", "vimdoc",
    "java",
    "kotlin",
    "rust",
    "sql",
    "bash",
    "cmake",
    "cpp",
    "css",
    "yaml",
    "regex",
    "python",
    "ruby",
    "dockerfile",
    "diff",
    "dot",
    "ebnf",
    "fish",
    "git_config", "git_rebase", "gitattributes", "gitcommit", "gitignore",
    "go",
    "html",
    "ini",
    "javascript",
    "jq",
    "json",
    "yaml",
    "latex",
    "lua", "luadoc", "luap",
    "llvm",
    "make",
    "markdown", "markdown_inline",
    "ninja",
    "perl",
    "php",
    "proto",
    "query",
    "po",
    "scala",
    "scss",
    "toml",
    "typescript",
    "verilog"
  },
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false
  },
  indent = { enable = false },
}

local ts = require('nvim-treesitter')

vim.opt.foldmethod = "expr"
vim.opt.foldexpr = ts.foldexpr
vim.opt.foldenable = false

local l= require('lspconfig')

l.rust_analyzer.setup {}
l.jdtls.setup {}
l.kotlin_language_server.setup {}
l.gopls.setup {}
l.bashls.setup {}
l.lua_ls.setup {
  settings = {
    Lua = {
      diagnostics = {
        globals = {'vim'}
      }
    }
  }
}
l.yamlls.setup {}
l.pylsp.setup {}
l.cssls.setup {}
l.html.setup {}
l.ccls.setup {}
l.ansiblels.setup {}
l.texlab.setup {}
l.sqlls.setup {}
l.rubocop.setup {}
l.lemminx.setup {}
l.dockerls.setup {}
-- todo
-- l.docker_compose_language_service.setup {}


local null_ls = require("null-ls")
local ca   = null_ls.builtins.code_actions
local cmp  = null_ls.builtins.completion
local diag = null_ls.builtins.diagnostics
local fmt  = null_ls.builtins.formatting

null_ls.setup({
  sources = {

    ca.eslint,
    ca.ltrs,
    ca.shellcheck,
    ca.gitsigns,

    cmp.tags,
    cmp.spell,

    --diag.rubocop,
    diag.ktlint,
    diag.ansiblelint,
    diag.fish,
    diag.vacuum,
    diag.yamllint,
    diag.gitlint,

    fmt.ruff,
    fmt.rustfmt,
    fmt.shellharden,
    fmt.sqlfluff,
    fmt.prettier,
    fmt.protolint,
    fmt.rubocop,
    fmt.uncrustify,
    fmt.xmllint,
  }
})

