vim.g.mapleader = ' '

vim.opt.list = true
vim.opt.listchars = 'eol:↵,tab:  ⇥,nbsp:␣,trail:·,precedes:«,extends:»'

vim.opt.expandtab = true
vim.opt.shiftwidth = 2
vim.opt.tabstop = 2
vim.opt.shell = "/bin/bash"
vim.opt.mouse = ''
vim.g.loaded_perl_provider = 0

vim.wo.number = true
vim.opt.termguicolors = true

vim.api.nvim_set_hl(0, "Normal", { bg="Black" })

require('plugins')
require('setup-languages')
require('telescope').load_extension('fzf')
require('keymap')
