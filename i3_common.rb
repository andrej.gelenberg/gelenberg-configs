# frozen_string_literal: true

require_relative 'lib/i3'

dirs = {
  left:  :h,
  down:  :j,
  up:    :k,
  right: :l
}

font 'pango:BlexMono Nerd Font Mono Text 10'
floating_modifier :Mod1

bind(:XF86AudioMute        ) { exec! 'pactl set-sink-mute   @DEFAULT_SINK@   toggle' }
bind(:XF86AudioRaiseVolume ) { exec! 'pactl set-sink-volume @DEFAULT_SINK@   +1000'  }
bind(:XF86AudioLowerVolume ) { exec! 'pactl set-sink-volume @DEFAULT_SINK@   -1000'  }
bind(:XF86AudioMicMute     ) { exec! 'pactl set-source-mute @DEFAULT_SOURCE@ toggle' }
bind(:XF86MonBrightnessUp  ) { exec! 'brightnessctl    set +5%'                      }
bind(:XF86MonBrightnessDown) { exec! 'brightnessctl -n set 5%-'                      }
bind(:XF86WLAN             ) { exec! 'rfkill toggle wlan'                            }

bind :Mod4 do
  bind(:f)      { fullscreen :toggle            }
  bind(:space)  { focus :mode_toggle            }
  bind(:p)      { focus :parent                 }
  bind(:c)      { focus :child                  }
  bind(:equal)  { move  :scratchpad             }
  bind(:r)      { mode  :resize                 }
  bind(:w)      { cmd   'sticky toggle'         }

  bind(:m)      { exec! 'loginctl lock-session' }
  bind(:Return) { exec  'kitty'                 }

  bind(:F1)     { exec  'kitty'                 }
  bind(:F2)     { exec  'firefox'               }
  bind(:F3)     { exec  'chromium'              }
  bind(:F4)     { exec  'thunderbird'           }
  bind(:F5)     { exec  'yt'                    }

  # bind(:bracketright) { exec! '~/bin/tablet_relative' }
  # bind(:bracketleft)  { exec! '~/bin/tablet_absolute' }

  [%i[s stacking], %i[t tabbed], [:d, 'toggle split']].each do |key, lt|
    bind(key) { layout lt }
  end

  [[:minus, :h], [:bar, :v]].each do |key, splt|
    bind(key) { split splt }
  end

  bind :Shift do
    bind(:q)     { kill    }
    bind(:c)     { reload  }
    bind(:r)     { restart }
    bind(:x)     { i3exit  }
    bind(:equal) { scratchpad :show }

    bind(:space) { floating :toggle }
  end

  %i[left down up right].each do |i|
    letter = dirs[i]
    cap = i.to_s.capitalize

    bind(letter) { focus i }
    bind(cap)    { focus i }

    bind :Shift do
      bind(letter) { move i }
      bind(cap)    { move i }
    end
  end

  (1..10).each do |i|
    n = ('0'.ord + (i % 10)).chr

    bind(n)      { workspacen i }
    bind(:Shift) { bind(n) { move_workspacen i } }
  end
end

step = '10 px or 10 ppt'
sstep = '1px or 1 ppt'
mode :resize do
  [
    [:width,  [%i[left shrink], %i[right grow  ]]],
    [:height, [%i[down grow  ], %i[up    shrink]]]
  ].each do |dim|
    dim[1].each do |i|
      letter = dirs[i[0]]
      cap = i[0].to_s.capitalize

      bind(letter) { resize i[1], dim[0], step }
      bind(cap)    { resize i[1], dim[0], step }
      bind :Shift do
        bind(letter) { resize i[1], dim[0], sstep }
        bind(cap)    { resize i[1], dim[0], sstep }
      end
    end
  end

  [:Return, :Escape, 'Mod4+r'].each do |i|
    bind(i) { mode :default }
  end
end

bar do
  status_command 'i3status-rs ~/.config/i3status/config.toml'
end
