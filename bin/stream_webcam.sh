#!/bin/sh
v4l2-ctl -c gain_automatic=0 -c gain=0 -c auto_exposure=1 -c exposure=100
exec gst-launch-1.0 \
    v4l2src device=/dev/video0 \
  ! "video/x-raw,framerate=30/1" \
  ! videoconvert \
  ! vaapih264enc keyframe-period=30 rate-control=4 \
  ! h264parse \
  ! flvmux streamable=true \
  ! rtmpsink location=rtmp://127.0.0.1:1935/webcam/1d5b9aab-94a0-43bd-ac17-2dc417ea7a94.stream
