#!/bin/sh
set -x
sudo mksquashfs / /mnt/bunker/backup/regular/$(date +backup_%Y_%m_%d_%H_%M.squash) -not-reproducible -comp zstd -b 1M -e /proc /dev /sys /mnt /tmp /run
