#!/bin/bash

set -x
cd ~/gelenberg-configs/3dprinter
docker compose stop

cd ~/sources
for i in klipper  klipper_ender_board
do
  echo "update $i"
  pushd "$i"

  git pull
  make clean && make
  ./flash.sh

  popd
done

cd ~/gelenberg-configs/3dprinter
docker compose pull
docker compose build --pull
docker compose up -d
