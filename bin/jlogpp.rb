#!/usr/bin/ruby

require 'json'

json_re = /^([^{]*)({.*})\s*$/

while line = readline
  m = json_re.match line
  if m.nil?
    puts line
    next
  end

  begin
    jline = JSON.parse m[2]
    puts m[1]
    puts JSON.pretty_generate(jline)
  rescue
    puts line
  end

end
