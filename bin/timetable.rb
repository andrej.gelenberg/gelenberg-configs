#!/usr/bin/env ruby

require 'readline'

$start       = 8*60..12*60
$pause       = 30..60
$pause_start = 2*60..6*60
$worktime    = 8 * 60 
$worktime    = ($worktime - 10)..($worktime + 15)

$random = Random.new

def rnd(range)
  $random.rand(range)
end

def tstr(s)
  sprintf "%2d:%02d", s/60, s%60
end

def get_work_day

  work_before_pause = rnd($pause_start)
  pause = rnd($pause)
  work_time = rnd($worktime)

  work_start  = rnd($start)
  pause_start = work_start + work_before_pause
  pause_end   = pause_start + pause
  work_end    = pause_end + (work_time - work_before_pause)



  puts "work: #{tstr work_start} - #{tstr work_end} pause: #{tstr pause_start} - #{tstr pause_end} (work time: #{tstr work_time} + pause #{tstr pause})"
end


while true
  get_work_day
 Readline.readline "hit enter...", false
end

