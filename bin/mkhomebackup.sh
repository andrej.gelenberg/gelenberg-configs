#!/bin/sh
set -xe
backup_local_path="/mnt/bunker/backup"
backup_file_name="$backup_local_path/$(date +"%Y_%m_%d")_home.squash"
exclude_file="$HOME/gelenberg-configs/backup_exclude_list.txt"
remote_backup_server="10.0.0.1:/mnt/storage/backup/manual/"
mksquashfs ~ "$backup_file_name" -comp xz -noappend -wildcards -ef "$exclude_file"
scp "$backup_file_name" "$remote_backup_server"
