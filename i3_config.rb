#!/usr/bin/ruby
# frozen_string_literal: true

require_relative 'i3_common'

exec! 'feh --bg-scale ~/.bg'
exec! 'xss-lock --transfer-sleep-lock -- i3lock -i ~/.bg --nofork'
