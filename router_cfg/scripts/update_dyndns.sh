#!/bin/sh
# first argument is either -4 or -6, to update ipv4 or ipv6 address correspondently
pw="$( < /etc/ddns/password )"
url="$( < /etc/ddns/url )"
hostname="$( cat /etc/ddns/hostname )"

echo "update dyndns for $hostname"
curl -f -s $1 \
  -u "$hostname:$pw" \
  --url-query "hostname=$hostname" \
  "$url"
