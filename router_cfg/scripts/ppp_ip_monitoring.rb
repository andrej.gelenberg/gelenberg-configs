#!/usr/bin/ruby

require 'net/http'
require 'resolv'
require 'uri'

check_period = 300

A    = Resolv::DNS::Resource::IN::A
AAAA = Resolv::DNS::Resource::IN::AAAA

def info msg
  puts "#{ Time.now.to_i }: Info: #{ msg }"
end

def error msg
  STDERR.puts "#{ Time.now.to_i }: Error: #{ msg }"
end

def res2ipfam res
  return res == Resolv::DNS::Resource::IN::AAAA
end

def also target, *args, &block
  target.instance_exec *args, &block
  target
end

class Timer
  def initialize delay = nil
    debug { "New timer" }
    @mutex   = Thread::Mutex.new
    @trigger = Thread::ConditionVariable.new
    @timeout = delay.nil? ? 0 : Time.now.to_i + delay

    @thread = Thread.new do
      while true
        debug { "timer: wait for timeout #{ @timeout }" }
        @mutex.synchronize do
          @trigger.wait @mutex if @timeout == 0

          while true
            now = Time.now.to_i
            break if now >= @timeout
            @mutex.sleep @timeout - now
          end

          @timeout = 0
        end

        debug { "timer trigger" }
        yield self

      end
    end
  end

  def rewind delay
    debug { "timer rewind #{ delay }" }
    raise "timer ist closed" if @thread.nil?

    @mutex.synchronize do
      old_timeout = @timeout
      @timeout = Time.now.to_i + delay
      # wake up either if timer is currently stopped
      # or if new wakeup time is sooner than the old one.
      # need to wake up to reajust sleep anyways, but don't need
      # to schedule the thread fo no reason
      if old_timeout == 0 || @timeout < old_timeout
        @thread.wakeup
      end
    end
  end

  def close
    @thread.kill
    @thread = nil
  end

  def closed?
    @thread.nil?
  end
end

class DDNS
  def initialize url, pwd
    @url      = URI(url)
    @pwd      = pwd

    @http = Net::HTTP.new @url.host, @url.port
    @http.use_ssl = @url.scheme == 'https'
    #@http.set_debug_output STDOUT

    info <<~END
      New DDNS instance
        url: #{ @url }
    END
  end

  def session
    @http.start { yield }
  end

  def update hostname, addrs
    return session { update hostname, addrs } unless @http.started?

    info "update #{ hostname } DNS with addresses #{ addrs.to_a }"

    ret = false
    @url.query = "hostname=#{ hostname }&myip=#{ addrs.join ',' }"
    debug { "update requiest uri: #{ @url.request_uri }" }
    req = Net::HTTP::Get.new @url.request_uri
    req.basic_auth hostname, @pwd

    debug { "send request #{ req }" }
    @http.request req do |res|
      debug { "response #{ res }" }
      if res.is_a? Net::HTTPSuccess
         info "update successfull: #{ res.body }"
         ret = true
      else
         info "update failed #{ res.status }: #{ res.body }"
      end
    end

    ret
  end
end

def get_if_addresses ifname
  ifname = ifname.to_s
  ret = Set.new

  Socket.getifaddrs.each do |i|
    next if i.name != ifname
    i.addr.instance_eval do
      next if nil?
      if ipv4?
        next if ipv4_loopback? || ipv4_private? || ipv4_multicast?
      elsif ipv6?
        next if ipv6_loopback? || ipv6_linklocal? || ipv6_multicast? || ipv6_sitelocal?
      else
        next
      end
      ret << ip_address
    end
  end

  debug { "got public addresses for interface #{ ifname }: #{ ret }" }

  ret
end

class IpUpdateMonitor
  def initialize ifname, &block
    info "start #{ ifname } interface monitoring service"

    @match_re = %r{
      ^\d+:\s+#{ ifname }\s+inet
      (
        \s+(?<ipv4>(?:\d{1,3}\.){3}\d{1,3})
        |
        6\s+(?<ipv6>[^\\s/]+)
      )
      (?:/\d+)?\s+ # maks
      (?:\S.*\s+)?scope\s+global
      (?:\s+(\S.*\s+)?(?<tentative>tentative))?
      .*$
    }x

    run &block if block_given?
  end

  def run
    IO.popen("ip monitor address").each_line do |line|
      match = line.match @match_re
      next if match.nil?
      next unless match[:tentative].nil? if match[:ipv6]

      yield *(match[:ipv4].nil? ? [:ipv6, match[:ipv6]] : [:ipv4, match[:ipv4]])
    end
  end
end

class DNSMonitor
  def initialize ifname, hostname, check_period = 300, update_cooldown_period = nil, &block
    @ifname = ifname
    @dns = Resolv::DNS.new
    @hostname = hostname

    @update_callback = block
    @check_period = check_period
    @cooldown = 0

    update_cooldown_period = check_period if update_cooldown_period.nil?
    @update_cooldown_period = update_cooldown_period

    @timer = Timer.new(0) { check }
  end

  def check
    debug { "timer check" }

    if_addrs = get_if_addresses @ifname

    if if_addrs.empty?
      error "no public ip addresses found for #{ @ifname } interface"
      return
    end

    dns_addrs = get_dns_addresses

    if dns_addrs != if_addrs
      begin
        debug { "run update callback" }
        if @update_callback.call @hostname, if_addrs
          set_cooldown_in @update_cooldown_period
        end

      rescue => e
        error "update callback failed: #{ e.full_message }"

      end
    end

    check_in @check_period
  end

  def get_dns_addresses
    ttl = 0
    addresses = Set.new

    [A,AAAA].each do |res_class|
      @dns.each_resource @hostname, res_class do |res|
        debug { "got DNS record for #{ @hostname }: address #{ res.address } ttl #{ res.ttl }" }

        if res.ttl > ttl
          ttl = res.ttl
          set_cooldown_in ttl
        end

        addresses << res.address.to_s
      end
    end

    debug { "got addresses for #{ @hostname } from DNS: #{ addresses }" }

    return addresses
  end

  def set_cooldown new_cooldown
    if new_cooldown > @cooldown
      @cooldown = new_cooldown
      debug { "new cooldown #{ @cooldown }" }
    end
  end

  def set_cooldown_in sec
    set_cooldown Time.now.to_i + sec
  end

  def check_in sec
    now = Time.now.to_i
    @timer.rewind now + sec > @cooldown ? sec : @cooldown - now
  end
end


if ARGV[0] == '-d'
  def debug
    STDERR.puts "#{ Time.now.to_i }: Debug: #{ yield }"
  end

  debug { 'debugging enableg' }

else
  def debug
  end

end

ddns = DDNS.new(
    File.read('/etc/ddns/url').chomp,
    File.read('/etc/ddns/password').chomp,
)

nic = File.read('/etc/ddns/nic').chomp

dnsmon = DNSMonitor.new nic, File.read('/etc/ddns/hostname').chomp, 300, 600 do |hostname, addresses|
  ddns.update hostname, addresses
end

IpUpdateMonitor.new nic do |fam, addr|
  dnsmon.check_in 1
end
