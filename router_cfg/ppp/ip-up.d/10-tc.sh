#!/bin/sh

logger "setup traffic control for $1"
modprobe ifb
ip link set dev ifb0 down
tc qdisc del dev ifb0 root
tc qdisc del dev ifb0 ingress
ip link set dev ifb0 up
tc qdisc add dev ifb0 root cake bandwidth 220mbit nat besteffort regional
tc qdisc add dev $1 root cake bandwidth 45mbit regional
tc qdisc add dev $1 handle ffff: ingress
tc filter add dev $1 parent ffff: protocol ip matchall action mirred egress redirect dev ifb0
