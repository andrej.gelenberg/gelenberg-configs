#!/bin/sh

logger "bringign up hurricane electric ipv6 tunnel up"
logger "he response: $(curl -s https://a:b@ipv4.tunnelbroker.net/nic/update?hostname=825965)" &
ip tunnel add he-ipv6 mode sit remote 216.66.86.114 local $4 ttl 255
ip link set he-ipv6 up
