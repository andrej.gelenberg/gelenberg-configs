# frozen_string_ligeral: true

def font(face)
  puts "font #{face}"
end

def exec(cmd)
  puts "exec #{cmd}"
end

def exec!(cmd)
  puts "exec --no-startup-id #{cmd}"
end

def floating_modifier(arg)
  puts "floating_modifier #{arg}"
end

def bind(key, &block)
  Bind.new key, &block
end

def mode(name)
  puts "mode \"#{name}\" {"
  yield
  puts '}'
end

def bar(&block)
  Bar.new(&block)
end

def input(type, &block)
  Input.new(type, &block)
end

def output(match, &block)
  Output.new(match, &block)
end

class Input
  def initialize(type, &block)
    puts "input #{type} {"
    instance_eval(&block)
    puts '}'
  end

  def method_missing(cmd, *args)
    if respond_to_missing? cmd, true
      puts "  #{cmd} \"#{args[0]}\""
    else
      super
    end
  end

  MISSING_RE = /xkb_(layout|model|variant|options)/.freeze

  def respond_to_missing?(cmd, include_private = false)
    cmd =~ MISSING_RE || super
  end
end

class Bar
  def initialize(&block)
    puts 'bar {'
    instance_eval(&block)
    puts '}'
  end

  def status_command(cmd)
    puts "  status_command #{cmd}"
  end
end

class Bind
  def self.defcmd(c)
    define_method c do |args|
      cmd "#{c} #{args}"
    end
  end

  def self.simplecmd(c)
    define_method c do
      cmd c
    end
  end

  %i[
    fullscreen exec focus
    split move layout floating
    scratchpad workspace
  ].each do |i|
    defcmd i
  end

  %i[kill reload restart].each do |i|
    simplecmd i
  end

  def initialize(cntx, &block)
    @cntx = cntx
    instance_exec(&block) if block_given?
  end

  def cmd(c)
    puts "bindsym #{@cntx} #{c}"
  end

  def bind(key, &block)
    Bind.new "#{@cntx}+#{key}", &block
  end

  def workspacen(n)
    cmd "workspace number #{n}"
  end

  def move_workspacen(n)
    cmd "move container to workspace number #{n}"
  end

  def i3exit
    cmd :exit
  end

  def mode(m)
    cmd "mode \"#{m}\""
  end

  def resize(act, dim, step)
    cmd "resize #{act} #{dim} #{step}"
  end

  def shrink(dim, step)
    resize :shrink, dim, step
  end

  def grow(dim, step)
    resize :grow, dim, step
  end

  def exec!(to_exec)
    cmd "exec --no-startup-id #{to_exec}"
  end
end

class Output
  def initialize(match, &block)
    puts "output \"#{match}\" {"
    instance_eval(&block)
    puts '}'
  end

  def background(color_file, mode, fallback = '')
    puts "  background \"#{color_file}\" #{mode} #{fallback}"
  end

  alias bg background

  def pos(x, y)
    puts "  pos #{x} #{y}"
  end

  def res(x, y)
    puts "  res #{x} #{y}"
  end
end
