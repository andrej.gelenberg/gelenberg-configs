export PATH="$PATH:/opt/resolve/bin:~/bin"
export EDITOR=nvim
#export DOCKER_HOST="unix://$XDG_RUNTIME_DIR/podman/podman.sock"
#export GDK_BACKEND=wayland
#export QT_QPA_PLATFORM=wayland-egl
#export VK_ICD_FILENAMES=/usr/share/vulkan/icd.d/amd_icd64.json:/usr/share/vulkan/icd.d/amd_icd32.json
#export VK_ICD_FILENAMES=/usr/share/vulkan/icd.d/radeon_icd.x86_64.json:/usr/share/vulkan/icd.d/radeon_icd.i686.json
export TERMINAL=kitty

HISTCONTROL=ignoreboth

alias nw="tmux new-window"
alias grep="grep --color"
alias cgrep="grep --color=always"
alias cless="less -r"


esc() {
  printf "\033[$1m"
}

prompt_fn() {
  esc 94
  printf "%(%d.%m.%y %H:%M:%S)T" -1
  git_status="$(git rev-parse --abbrev-ref HEAD 2>/dev/null)"
  if [ -n "$git_status" ]
  then
    esc 33
    printf " git: %s" "$git_status"
  fi
  esc 36
}

#export PROMPT_COMMAND="prompt_fn"
#export PS1=" jobs: \j \033[34m\u@\h \033[36mhist: \! \033[34m\w\n\r\033[93m\# \033[97m\$ \033[39m"

[ -z "$SSH_AUTH_SOCK" ] && export SSH_AUTH_SOCK=~/.local/tmp/ssh.agent
