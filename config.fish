if test -e ~/bin
  #echo "set aliases"
  fish_add_path ~/bin/
end

if test -e ~/.cargo/bin
  fish_add_path ~/.cargo/bin
end

fish_add_path /usr/lib/jvm/default/bin

set -gx EDITOR nvim

if test -z "$SSH_AUTH_SOCK"
  set -gx SSH_AUTH_SOCK ~/.local/tmp/ssh.agent
end

if test -z "$XAUTHORITY"
  set -gx XAUTHORITY "$(echo ~/.Xauthority)"
end

set -gx XDG_CONFIG_HOME "$HOME/.config"
set -gx XDG_CACHE_HOME  "$HOME/.cache"
set -gx XDG_DATA_HOME   "$HOME/.local/share"
set -gx XDG_STATE_HOME  "$HOME/.local/state"
set -gx DOCKER_HOST     "unix://$XDG_RUNTIME_DIR/podman/podman.sock"
set -gx ROC_ENABLE_PRE_VEGA 1
set -gx _JAVA_AWT_WM_NONREPARENTING 1

function nw
  tmux new-window $argv
end

#echo "set autocomplete"
complete -c nw -a "(__fish_complete_subcommand)"

#echo "etc"
fish_vi_key_bindings

function grep
  command grep --color $argv
end

function cgrep
  command grep --color=always $argv
end

function tailf
  tial -f $argv
end

function ll
  ls -l $argv
end

function lh
  ls -lh $argv
end

function la
  ls -a $argv
end

function cless
  less -r
end

function firefox
  command firefox $argv &
  disown
end

function chrome
  google-chrome-stable $argv &
  disown
end

function idea
  intellij-idea-ultimate-edition $argv &
  disown
end

function thunderbird
  command thunderbird $argv &
  disown
end

function slack
  command slack $argv &
  disown
end
