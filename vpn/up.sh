#!/bin/bash

NS="ip netns exec $1"

set -x
echo "move $dev to net namespace $1"

ip netns add "$1"

$NS ip addr add 127.0.0.1/8 dev lo
$NS ip address add '::1/128' dev lo
$NS ip link set dev lo up

ip link set dev "$dev" up netns "$1" mtu "$link_mtu"

$NS ip address add \
    local "$ifconfig_local/$ifconfig_netmask" \
    ${ifconfig_remote:+peer "$ifconfig_remote"} \
    dev "$dev"

$NS ip address add \
    local "$ifconfig_ipv6_local/$ifconfig_ipv6_netbits" \
    ${ifconfig_ipv6_remote:+peer "$ifconfig_ipv6_remote"} \
    dev "$dev"

$NS ip route add default dev "$dev" ${route_vpn_gateway:+via "$route_vpn_gateway"}
$NS ip -6 route add default dev "$dev"

for net in ${!route_ipv6_network_*}
do
  gw="route_ipv6_gateway_${net#route_ipv6_network_}"
  gw="${!gw}"
  net="${!net}"
  $NS ip -6 route add "$net" dev "$dev" ${gw:+via "$gw"}
done


#env | sort
#ip netns exec "$1" tracepath 8.8.8.8
